import Slider from "react-slick";
import Style from "../../../App.module.css";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const Carousel = () =>{
   const settings = {
    infinite : true,
    speed : 500,
    slidesToShow : 1,
    slidesToScroll : 1,
  
   };

    return(

        <div>
          <Slider {...settings}>
              <div className={Style.Slide1}>
                  <div className="row mt-4">
                    <div className="col-sm-6" style={{paddingTop : "200px"}}>
                      <h1 className={Style.h1SLide1}>Welcome !</h1> 
                      <p className={Style.pSlide1}>Autmn Collection Just Arrived  </p>
                      <button className="btn btn-primary">Check Our Collection</button>
                    </div>
                    <div className="col-sm-6"></div>
                  </div>
              </div>
              
          </Slider>
        </div>
    
    )
}

export default Carousel