import Logo from "./LogoComponent/Logo"
import Navbar from "./NavbarComponent/Navbar"
import Style from "../../App.module.css";
import SearchBar from "./SearchBarComponent/SearchBar";
const Header = () => {
    return(
        <>
            <div className={Style.Header}>
                <div className={Style.topInfo}>
                    <div className="container">
                        <div className={Style.topInfoContent}>
                            <div className={Style.topInfoLeft}>
                                <p style={{fontStyle : "italic"}} className="text-white">Freeshipping from 700,000 vnđ paid ~</p>
                            </div>
                            <div className={Style.topInfoRight}>
                                <a className={Style.aTopinfoRight} href="#">
                                    About Us 
                                </a>
                                <a className={Style.aTopinfoRight} href="#">
                                    Contact
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={Style.headerTop}>
                    <div className="container">
                        <div>
                            <div className={Style.headerAll}>
                                <Logo/>
                                <SearchBar/>
                                <Navbar/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </>
    )
}

export default Header