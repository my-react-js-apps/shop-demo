import Style from "../../../App.module.css";
const InfoFooter3= () =>{
    return(
        <>
            <div className="col-sm-3 text-white  mt-4">
                <h4 style={{paddingLeft : "30px"}}>SUPPORT</h4>
                <ul className={Style.ulStyleType} style={{marginRight :"30px"}}>
                    <li>
                        <a className={Style.aLiStyle} href="#">Help Center</a>
                    </li>
                    <li>
                        <a className={Style.aLiStyle} href="#">Contacts Us</a>
                    </li>
                    <li>
                        <a className={Style.aLiStyle} href="#">Product Help</a>
                    </li>
                    <li>
                        <a className={Style.aLiStyle} href="#">Warranty</a>
                    </li>
                    <li>
                        <a className={Style.aLiStyle} href="#">Order Status</a>
                    </li>
                </ul>
            </div>
        </>
    )
}

export default InfoFooter3