import { AiOutlineBell } from "react-icons/ai";
import {BsPersonCircle} from "react-icons/bs";
import {BsCartCheck} from "react-icons/bs";
import Style from "../../../App.module.css";
const Navbar = () =>{
    return(
        <>
            <div >
               <div className={Style.divNavBar}>
                    <a href="#" className={Style.aNavBarHeader}><AiOutlineBell/></a>
                    <a href="#" className={Style.aNavBarHeader}><BsPersonCircle/></a>
                    <a href="#" className={Style.aNavBarHeader}><BsCartCheck/></a>
               </div>
            </div>
        </>
    )
}

export default Navbar