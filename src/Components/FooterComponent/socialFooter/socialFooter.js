import Style from "../../../App.module.css";
import {BsFacebook} from "react-icons/bs";
import {AiFillInstagram} from "react-icons/ai";
import {BsYoutube} from "react-icons/bs";
import {BsTwitter} from "react-icons/bs";
const SocialFooter = () =>{
    return(
        <>
            <div className="col-sm-3 text-white mt-4">
                <a className={Style.aSocialFooter} href="#">
                    <h3>Social Links</h3>
                </a>
                <div>
                    <a className={Style.aSocialIcon} href="#"><BsFacebook/></a>
                    <a className={Style.aSocialIcon} href="#"><AiFillInstagram/></a>
                    <a className={Style.aSocialIcon} href="#"><BsYoutube/></a>
                    <a className={Style.aSocialIcon} href="#"><BsTwitter/></a>
                </div>
            </div>
        </>
    )
}

export default SocialFooter