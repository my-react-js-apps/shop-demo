import Style from "../../App.module.css";
import InfoFooter1 from "./infoFooter/infoFooter1";
import InfoFooter2 from "./infoFooter/infoFooter2";
import InfoFooter3 from "./infoFooter/infoFooter3";
import SocialFooter from "./socialFooter/socialFooter";
const Footer = () =>{
    return (
        <>
            <div className={Style.footerBottom}>
                <div className="container">
                    <div className="row">
                        <InfoFooter1/>
                        <InfoFooter2/>
                        <InfoFooter3/>
                        <SocialFooter/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Footer